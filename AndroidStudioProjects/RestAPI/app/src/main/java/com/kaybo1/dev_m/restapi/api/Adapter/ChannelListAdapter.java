package com.kaybo1.dev_m.restapi.api.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaybo1.dev_m.restapi.R;
import com.kaybo1.dev_m.restapi.api.model.home.ChannelList;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Administrator on 2017-09-25.
 */

public class ChannelListAdapter extends ArrayAdapter<ChannelList>{
    List<ChannelList> ChannelLists;
    Context context;
    private LayoutInflater mInflater;

    public ChannelListAdapter(Context context, List<ChannelList> objects){
        super(context,0,objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        ChannelLists = objects;
    }
    public ChannelList getItem(int position){
        return ChannelLists.get(position);
    }
    public View getView(int position, View convertView, ViewGroup parent){
        final ViewHolder vh;
        if(convertView == null){
            View view = mInflater.inflate(R.layout.activity_display,parent,false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        }else{
            vh = (ViewHolder) convertView.getTag();
        }
        ChannelList item = getItem(position);

        vh.textChannelId.setText(item.getChannelId());
        vh.textContent.setText(item.getContent());
        vh.textName.setText(item.getName());
        vh.textPostId.setText(item.getPostId());
        vh.textTitle.setText(item.getTitle());
        vh.textType.setText(item.getType());
        Picasso.with(context).load(item.getThumbImageUrl()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        return vh.rootView;
    }
    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView textChannelId;
        public final TextView textContent;
        public final TextView textName;
        public final TextView textPostId;
        public final TextView textTitle;
        public final TextView textType;
        public ImageView imageView;

        private ViewHolder(RelativeLayout rootView, TextView textChannelId, TextView textContent, TextView textName, TextView textPostId,TextView textTitle,TextView textType, ImageView imageView){
            this.rootView = rootView;
            this.textChannelId = textChannelId;
            this.textContent = textContent;
            this.textName = textName;
            this.textPostId = textPostId;
            this.textTitle = textTitle;
            this.textType = textType;
            this.imageView = imageView;

        }
        public static ViewHolder create(RelativeLayout rootView){
            TextView textChannelId = (TextView) rootView.findViewById(R.id.textChannelId);
            TextView textContent = (TextView) rootView.findViewById(R.id.textContent);
            TextView textName = (TextView) rootView.findViewById(R.id.textName);
            TextView textPostId = (TextView) rootView.findViewById(R.id.textPostId);
            TextView textTitle = (TextView) rootView.findViewById(R.id.textTitle);
            TextView textType = (TextView) rootView.findViewById(R.id.textType);
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            return new ViewHolder(rootView,textChannelId,textContent,textName,textPostId,textTitle,textType,imageView);
        }


    }
}

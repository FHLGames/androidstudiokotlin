package com.kaybo1.dev_m.restapi.api.model.home;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017-09-22.
 */

public class homeListRepo {
    @SerializedName("info")
    Info info;

    public class Info{
        public String getRecommendTitle() {
            return recommendTitle;
        }

        public void setRecommendTitle(String recommendTitle) {
            this.recommendTitle = recommendTitle;
        }

        public String getRecommendThemeTitle() {
            return recommendThemeTitle;
        }

        public void setRecommendThemeTitle(String recommendThemeTitle) {
            this.recommendThemeTitle = recommendThemeTitle;
        }

        public String getRecommendThemeContent() {
            return recommendThemeContent;
        }

        public void setRecommendThemeContent(String recommendThemeContent) {
            this.recommendThemeContent = recommendThemeContent;
        }

        public String getCommunityTitle() {
            return communityTitle;
        }

        public void setCommunityTitle(String communityTitle) {
            this.communityTitle = communityTitle;
        }
        @SerializedName("recommendTitle")
        String recommendTitle;
        @SerializedName("recommendThemeTitle")
        String recommendThemeTitle;
        @SerializedName("recommendThemeContent")
        String recommendThemeContent;
        @SerializedName("communityTitle")
        String communityTitle;

        @SerializedName("bannnerList")
        public ArrayList<bannerList> bannerLists =new ArrayList<>();

        public ArrayList<bannerList> getBannerLists() {
            return bannerLists;
        }

        public void setBannerLists(ArrayList<bannerList> bannerLists) {
            this.bannerLists = bannerLists;
        }

        public class bannerList{
            public String getHomeBannerId() {
                return homeBannerId;
            }

            public void setHomeBannerId(String homeBannerId) {
                this.homeBannerId = homeBannerId;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public String getLinkUrl() {
                return linkUrl;
            }

            public void setLinkUrl(String linkUrl) {
                this.linkUrl = linkUrl;
            }

            public String getCreateDate() {
                return createDate;
            }

            public void setCreateDate(String createDate) {
                this.createDate = createDate;
            }


            @SerializedName("homeBannerId")
            String homeBannerId;
            @SerializedName("imageUrl")
            String imageUrl;
            @SerializedName("linkUrl")
            String linkUrl;
            @SerializedName("createDate")
            String createDate;

            @Override
            public String toString() {
                return "bannerList{" +
                        "homeBannerId='" + homeBannerId + '\'' +
                        ", imageUrl='" + imageUrl + '\'' +
                        ", linkUrl='" + linkUrl + '\'' +
                        ", createDate='" + createDate + '\'' +
                        '}';
            }
        }


        @Override
        public String toString() {
            return "Info{" +
                    "recommendTitle='" + recommendTitle + '\'' +
                    ", recommendThemeTitle='" + recommendThemeTitle + '\'' +
                    ", recommendThemeContent='" + recommendThemeContent + '\'' +
                    ", communityTitle='" + communityTitle + '\'' +
                    '}';
        }
    }
    public Info getInfo() {return info;}
}

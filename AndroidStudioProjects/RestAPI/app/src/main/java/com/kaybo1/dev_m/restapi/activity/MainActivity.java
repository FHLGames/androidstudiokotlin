package com.kaybo1.dev_m.restapi;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.kaybo1.dev_m.restapi.api.Adapter.ChannelListAdapter;
import com.kaybo1.dev_m.restapi.api.Adapter.ShortListAdapter;
import com.kaybo1.dev_m.restapi.api.model.home.ChannelList;
import com.kaybo1.dev_m.restapi.api.model.home.HomeListRepo;
import com.kaybo1.dev_m.restapi.api.model.home.ShortCutList;
import com.kaybo1.dev_m.restapi.api.service.HomeService;
import com.kaybo1.dev_m.restapi.api.util.InternetConnection;
import com.kaybo1.dev_m.restapi.api.util.RestClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MainActivity extends AppCompatActivity {
    private ListView channellistView;
    private ListView shortlistView;
    //private ListView ShortListView;
    private View parentView;

    private ArrayList<ChannelList> channelLists;
    private ArrayList<ShortCutList> shortCutLists;
    private ChannelListAdapter Channeladapter;
    private ShortListAdapter Shortadapter;
    HomeService homeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        channelLists = new ArrayList<>();
        shortCutLists = new ArrayList<>();
        parentView = findViewById(R.id.parentLayout);
        channellistView = (ListView)findViewById(R.id.channeListView);
        shortlistView = (ListView)findViewById(R.id.shortListView);
        //ShortListView = (ListView)findViewById(shortListView);
        channellistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int postion, long id) {
                Snackbar.make(parentView, channelLists.get(postion).getChannelId() + " => " + channelLists.get(postion).getName(), Snackbar.LENGTH_LONG).show();
            }
        });

        Toast toast = Toast.makeText(getApplicationContext(),R.string.string_click_to_load, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(@NonNull final View view) {

                if(InternetConnection.checkConnection(getApplicationContext())){
                    new Task().execute();
                    /*RestClient<HomeService> mowaRestClient;
                    HomeService homeService;
                    mowaRestClient = new RestClient<>();
                    homeService = mowaRestClient.getClinet(HomeService.class);
                    Call<HomeListRepo> call = homeService.repoContributors();
                    call.enqueue(new Callback<HomeListRepo>() {
                        @Override
                        public void onResponse(Call<HomeListRepo> call, Response<HomeListRepo> response) {
                            dialog.dismiss();
                            if(response.isSuccessful()){
                                channelLists = response.body().getChannelList();

                                adapter =  new ChannelListAdapter(MainActivity.this,channelLists);
                                listView.setAdapter(adapter);
                            }else{
                                Snackbar.make(parentView,R.string.string_thing_wrong,Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<HomeListRepo> call, Throwable t) {
                                dialog.dismiss();
                        }
                    });*/
                }else {
                    Snackbar.make(parentView,R.string.string_internet_connection_not_available,Snackbar.LENGTH_LONG).show();
                }
            }
            class Task extends AsyncTask<HomeService,HomeService,ArrayList<ChannelList>> {

                @Override
                protected ArrayList<ChannelList> doInBackground(HomeService... homeServices) {
                    RestClient<HomeService> mowaRestClient;
                    HomeService homeService;
                    mowaRestClient = new RestClient<>();
                    homeService = mowaRestClient.getClinet(HomeService.class);
                    Call<HomeListRepo> call = homeService.repoContributors();
                    call.enqueue(new Callback<HomeListRepo>() {
                        @Override
                        public void onResponse(Call<HomeListRepo> call, Response<HomeListRepo> response) {
                            final ProgressDialog dialog;
                            dialog = new ProgressDialog(MainActivity.this);
                            dialog.setTitle(getString(R.string.string_getter_json_title));
                            dialog.setMessage(getString(R.string.string_getting_json_message));
                            dialog.show();
                            if(response.isSuccessful()){

                                channelLists = response.body().getChannelList();
                                shortCutLists = response.body().getShortCutList();
                                Channeladapter =  new ChannelListAdapter(MainActivity.this,channelLists);
                                Shortadapter = new ShortListAdapter(MainActivity.this,shortCutLists);
                                dialog.dismiss();
                                channellistView.setAdapter(Channeladapter);
                                shortlistView.setAdapter(Shortadapter);
                                setListViewHeightBasedOnChildren(channellistView);
                                setListViewHeightBasedOnChildren(shortlistView);

                            }else{
                                Snackbar.make(parentView,R.string.string_thing_wrong,Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<HomeListRepo> call, Throwable t) {

                        }
                    });
                    return channelLists;
                }

                @Override
                protected void onPostExecute(ArrayList<ChannelList> result){
                    super.onPostExecute(result);


                }
            }
        });
        /*Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                getHomeList();
            }
        });*/
    }
    public void getHomeList() {
        channelLists = new ArrayList<>();
        RestClient<HomeService> mowaRestClient;
        HomeService homeService;
        mowaRestClient = new RestClient<>();
        homeService = mowaRestClient.getClinet(HomeService.class);
        Call<HomeListRepo> call = homeService.repoContributors();
        call.enqueue(new Callback<HomeListRepo>() {

            @Override
            public void onResponse(Call<HomeListRepo> call, Response<HomeListRepo> response) {
                channelLists = response.body().getChannelList();
                Log.d("HomeList",channelLists.get(0).toString());
            }

            @Override
            public void onFailure(Call<HomeListRepo> call, Throwable t) {
                Log.d("Error","error");
            }
        });

    }
    public void setListViewHeightBasedOnChildren(ListView listView){
        ListAdapter listAdapter = listView.getAdapter();
        if(listAdapter == null){
            return;
        }
        int totalHeight = 0;
        for(int i=0;i<listAdapter.getCount(); i++){
            View listItem = listAdapter.getView(i,null,listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() -1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}
